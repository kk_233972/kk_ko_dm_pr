package clean.code;

public class Division implements Operation {
    @Override
    public double evaluate(double firstNumber, double secondNumber) throws ArithmeticException {
        if (secondNumber != 0.0) {
            return firstNumber / secondNumber;
        } else {
            throw new ArithmeticException();
        }
    }
}
