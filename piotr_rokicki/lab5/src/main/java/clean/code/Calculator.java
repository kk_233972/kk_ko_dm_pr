package clean.code;

public class Calculator {
    private Operation currentOperation;

    public void setCurrentOperation(Operation currentOperation) {
        this.currentOperation = currentOperation;
    }

    public double evaluateExpression(double firstNumber, double secondNumber) {
        return this.currentOperation.evaluate(firstNumber, secondNumber);
    }
}
