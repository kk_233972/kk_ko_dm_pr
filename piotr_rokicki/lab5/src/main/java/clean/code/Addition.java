package clean.code;

public class Addition implements Operation {
    @Override
    public double evaluate(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }
}
