package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTest {

  Multiplication multiplication = new Multiplication();
  @Test
  void evaluate() {
    Assertions.assertEquals(2.0, this.multiplication.evaluate(1.0, 2.0), 0.01);
  }
}