package pl.tiuckd.io.util;

import java.util.Scanner;

public class CalculatorWriter {

  public double getNumber() throws Exception {
    double number = 0;
    try {
      number = Double.parseDouble(writeFromKeyboard());
    } catch (RuntimeException e) {
      throw new Exception();
    }
    return number;
  }

  private String writeFromKeyboard() {
    return new Scanner(System.in).next();
  }

  public char getOperationCharacter() {
    return writeFromKeyboard().toCharArray()[0];
  }
}
