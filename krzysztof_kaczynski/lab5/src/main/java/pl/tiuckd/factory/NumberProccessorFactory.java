package pl.tiuckd.factory;

import pl.tiuckd.exception.IllegalOperationException;
import pl.tiuckd.processor.*;

public class NumberProccessorFactory {

  public NumberProcessor getCalculator(char op, double numberOne, double numberTwo) {
    switch (op) {
      case '+':
        return new Adder(numberOne, numberTwo);
      case '-':
        return new Subtractor(numberOne, numberTwo);
      case '*':
        return new Multiplier(numberOne, numberTwo);
      case '/':
        return new Divider(numberOne, numberTwo);
      default:
        throw new IllegalOperationException();
    }
  }
}
