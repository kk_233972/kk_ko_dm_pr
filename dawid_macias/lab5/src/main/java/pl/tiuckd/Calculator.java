package pl.tiuckd;

import pl.tiuckd.exception.DivisionByZeroException;
import pl.tiuckd.exception.IllegalOperationException;
import pl.tiuckd.factory.NumberProccessorFactory;
import pl.tiuckd.io.util.CalculatorPrinter;
import pl.tiuckd.io.util.CalculatorWriter;
import pl.tiuckd.processor.NumberProcessor;
import pl.tiuckd.type.ErrorType;
import pl.tiuckd.type.NumberOrder;

public class Calculator {

  private NumberProcessor numberProcessor;
  private final CalculatorPrinter calculatorPrinter;
  private final CalculatorWriter calculatorWriter;
  private final NumberProccessorFactory numberProccessorFactory;

  public Calculator() {
    calculatorPrinter = new CalculatorPrinter();
    calculatorWriter = new CalculatorWriter();
    numberProccessorFactory = new NumberProccessorFactory();
  }

  public Calculator(
      CalculatorPrinter calculatorPrinter,
      CalculatorWriter calculatorWriter,
      NumberProccessorFactory numberProccessorFactory,
      NumberProcessor numberProcessor) {
    this.calculatorPrinter = calculatorPrinter;
    this.calculatorWriter = calculatorWriter;
    this.numberProccessorFactory = numberProccessorFactory;
    this.numberProcessor = numberProcessor;
  }

  public void runCalculator() {
    calculatorPrinter.printHelloMessage();
    calculatorPrinter.printNumber(NumberOrder.FIRST);
    double numberOne = getNumberFromUser();
    calculatorPrinter.printNumber(NumberOrder.SECOND);
    double numberTwo = getNumberFromUser();
    calculatorPrinter.printOperationCharacter();
    char operation = calculatorWriter.getOperationCharacter();
    double result = 0;
    try {
      result = operation(numberOne, operation, numberTwo);
      calculatorPrinter.printResultMessage(result);
    } catch (IllegalOperationException i) {
      calculatorPrinter.printErrorMessage(ErrorType.OPERATION);
    } catch (DivisionByZeroException d) {
      calculatorPrinter.printErrorMessage(ErrorType.DIVISION);
    }
  }

  private double getNumberFromUser() {
    double numberOne = 0;
    try {
      numberOne = calculatorWriter.getNumber();
    } catch (Exception e) {
      calculatorPrinter.printErrorMessage(ErrorType.NUMBER);
      numberOne = getNumberFromUser();
    }
    return numberOne;
  }

  private double operation(double numberOne, char op, double numberTwo) {
    numberProcessor = numberProccessorFactory.getCalculator(op, numberOne, numberTwo);
    return numberProcessor.getResult();
  }
}
