package com.tiuck.calculator.strategy;

public interface OperationStrategy {

    public double calculate(double a, double b);
}
