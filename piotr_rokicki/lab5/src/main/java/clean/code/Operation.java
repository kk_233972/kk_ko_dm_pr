package clean.code;

public interface Operation {
    double evaluate(double firstNumber, double secondNumber) throws ArithmeticException;
}
