package pl.tiuckd.processor;

public class Multiplier implements NumberProcessor {

  private final double numberOne;
  private final double numberTwo;

  public Multiplier(double numberOne, double numberTwo) {
    this.numberOne = numberOne;
    this.numberTwo = numberTwo;
  }

  @Override
  public double getResult() {
    return numberOne * numberTwo;
  }
}
