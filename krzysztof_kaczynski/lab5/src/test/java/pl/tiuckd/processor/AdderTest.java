package pl.tiuckd.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class AdderTest {

  @Test
  public void testGetResult_shouldReturnCorrectValue() {
    // given
    int numberOne = 5;
    int numberTwo = 5;
    int expected = numberOne + numberTwo;
    NumberProcessor numberProcessor = new Adder(numberOne, numberTwo);

    // when
    // then
    assertEquals(expected, numberProcessor.getResult());
  }
}
