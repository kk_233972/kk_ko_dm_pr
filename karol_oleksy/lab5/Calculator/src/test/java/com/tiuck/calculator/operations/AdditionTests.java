package com.tiuck.calculator.operations;


import com.tiuck.calculator.operations.Addition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdditionTests {

    @Test
    public void testCalculate_shouldReturnSumOfGivenNumbers() {
        Addition addition = new Addition();

        assertEquals(8, addition.calculate(3, 5), 2);
    }
}
