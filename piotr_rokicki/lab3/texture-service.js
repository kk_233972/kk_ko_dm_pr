class TextureService {
    loadTexturePlaceholder(gl, textureConfig) {
        const width = 1;
        const height = 1;
        const border = 0;
        const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
        gl.texImage2D(gl.TEXTURE_2D, textureConfig.level, textureConfig.internalFormat,
            width, height, border, textureConfig.srcFormat, textureConfig.srcType,
            pixel);
    }

    loadTexture(gl, textureUrl) {
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);

        const textureConfig = {
            level: 0,
            internalFormat: gl.RGBA,
            srcFormat: gl.RGBA,
            srcType: gl.UNSIGNED_BYTE
        };

        this.loadTexturePlaceholder(gl, textureConfig);

        const image = new Image();
        var isPowerOf2 = this.isPowerOf2;
        image.onload = function() {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, textureConfig.level, textureConfig.internalFormat,
                textureConfig.srcFormat, textureConfig.srcType, image);

            if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
                gl.generateMipmap(gl.TEXTURE_2D);
            } else {
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }
        };
        image.src = textureUrl;

        return texture;
    }

    isPowerOf2(value) {
        return (value & (value - 1)) === 0;
    }
}

