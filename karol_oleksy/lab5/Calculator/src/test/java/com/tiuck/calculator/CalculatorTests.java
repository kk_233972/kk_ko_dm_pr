package com.tiuck.calculator;

import com.tiuck.calculator.operations.Addition;
import com.tiuck.calculator.strategy.OperationStrategy;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTests {

    @Test
    public void testSet_shouldSetOperationStrategy() {
        Calculator calculator = new Calculator();
        OperationStrategy strategy = new Addition();
        calculator.set(strategy);
        assertTrue(calculator.getStrategy() instanceof Addition);
    }

    @Test
    public void testCountResult_shouldCorrectResult() throws FileNotFoundException {

        Calculator calculator = new Calculator();
        OperationStrategy strategy = new Addition();
        calculator.set(strategy);
        calculator.countResult(4, 6);
        assertEquals(10, calculator.getResult(), 2);
    }

    @Test
    public void testGetStrategy_shouldReturnInstanceOfAdditionStategy() {
        Calculator calculator = new Calculator();
        OperationStrategy strategy = new Addition();
        calculator.set(strategy);
        assertTrue(calculator.getStrategy() instanceof Addition);
    }
}
