package pl.tiuckd.type;

public enum NumberOrder {
  FIRST("pierwsza"),
  SECOND("druga");

  private final String input;

  NumberOrder(String input) {
    this.input = input;
  }

  public String getInput() {
    return input;
  }
}
