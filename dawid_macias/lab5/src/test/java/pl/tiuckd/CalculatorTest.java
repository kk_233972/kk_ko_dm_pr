package pl.tiuckd;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import pl.tiuckd.factory.NumberProccessorFactory;
import pl.tiuckd.io.util.CalculatorPrinter;
import pl.tiuckd.io.util.CalculatorWriter;
import pl.tiuckd.processor.Adder;
import pl.tiuckd.processor.NumberProcessor;

public class CalculatorTest {

  @Test
  public void testRunCalculator_shouldInvokeMethod() throws Exception {
    // given
    NumberProccessorFactory numberProccessorFactory = mock(NumberProccessorFactory.class);
    NumberProcessor numberProcessor = mock(NumberProcessor.class);
    CalculatorWriter calculatorWriter = mock(CalculatorWriter.class);
    CalculatorPrinter calculatorPrinter = mock(CalculatorPrinter.class);
    Calculator calculator =
        new Calculator(
            calculatorPrinter, calculatorWriter, numberProccessorFactory, numberProcessor);

    // when
    when(numberProcessor.getResult()).thenReturn(10.0);
    when(calculatorWriter.getNumber()).thenReturn(5.0);
    when(numberProccessorFactory.getCalculator('+', 5.0, 5.0)).thenReturn(new Adder(5.0, 5.0));
    when(calculatorWriter.getOperationCharacter()).thenReturn('+');
    calculator.runCalculator();

    // then
    verify(numberProccessorFactory, times(1)).getCalculator('+', 5.0, 5.0);
    verify(calculatorWriter, times(2)).getNumber();
    verify(calculatorWriter, times(1)).getOperationCharacter();
  }
}
