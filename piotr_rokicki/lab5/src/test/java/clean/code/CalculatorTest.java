package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

  private Calculator calculator = new Calculator();
  private Addition additionStub = new Addition();

  @Test
  void evaluateExpression() {
    calculator.setCurrentOperation(additionStub);
    Assertions.assertEquals(4.0, calculator.evaluateExpression(2.0, 2.0));
  }
}