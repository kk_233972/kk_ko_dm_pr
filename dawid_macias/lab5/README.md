# Wyniki

Po napisaniu testów pokrycie kodu testami wyniosło 81%.
Do repozytorium dołączony został pliki html prezentujący wyniki przed i po napisaniu testów.
Wyniki znajdują się w folderze result.
Do wykonania zadania wykorzystano JUnit5, oraz Mockito.
