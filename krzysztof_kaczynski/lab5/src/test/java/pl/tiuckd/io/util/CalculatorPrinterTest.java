package pl.tiuckd.io.util;

import org.junit.jupiter.api.Test;
import pl.tiuckd.type.ErrorType;
import pl.tiuckd.type.NumberOrder;

public class CalculatorPrinterTest {

  private CalculatorPrinter calculatorPrinter = new CalculatorPrinter();

  @Test
  public void testPrintHelloMessage_shouldInvoke() {
    // when
    // then
    calculatorPrinter.printHelloMessage();
  }

  @Test
  public void testPrintOperationCharacter_shouldInvoke() {
    // when
    // then
    calculatorPrinter.printOperationCharacter();
  }

  @Test
  public void testPrintNumber_shouldInvoke() {
    // when
    // then
    calculatorPrinter.printNumber(NumberOrder.FIRST);
    calculatorPrinter.printNumber(NumberOrder.SECOND);
  }

  @Test
  public void testPrintResultMessage_shouldInvoke() {
    // when
    // then
    calculatorPrinter.printResultMessage(5);
  }

  @Test
  public void testPrintErrorMessage_shouldInvoke() {
    // when
    // then
    calculatorPrinter.printErrorMessage(ErrorType.OPERATION);
    calculatorPrinter.printErrorMessage(ErrorType.DIVISION);
    calculatorPrinter.printErrorMessage(ErrorType.NUMBER);
  }
}
