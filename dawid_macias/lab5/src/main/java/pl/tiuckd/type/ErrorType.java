package pl.tiuckd.type;

public enum ErrorType {
  OPERATION,
  NUMBER,
  DIVISION
}
