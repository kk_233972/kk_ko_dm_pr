package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdditionTest {

  Addition addition = new Addition();
  @Test
  void evaluate() {
    Assertions.assertEquals(3.0, this.addition.evaluate(1.5, 1.5), 0.01);
  }
}