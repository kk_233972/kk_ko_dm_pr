main();

function main() {
  const canvas = document.querySelector('#glcanvas');

  const webGL = new WebGL(canvas.getContext('webgl'), vertexShaderSource, fragmentShaderSource,
      new BuffersService(cube), new TextureService());
  if (!webGL.constructedProperly()) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  webGL.init();

  webGL.startDrawing();
}
