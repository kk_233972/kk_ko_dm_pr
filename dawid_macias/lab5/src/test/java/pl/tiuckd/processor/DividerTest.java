package pl.tiuckd.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import pl.tiuckd.exception.DivisionByZeroException;

public class DividerTest {

  @Test
  public void testGetResult_shouldReturnCorrectValue() {
    // given
    int numberOne = 5;
    int numberTwo = 5;
    int expected = numberOne / numberTwo;
    NumberProcessor numberProcessor = new Divider(numberOne, numberTwo);

    // when
    // then
    assertEquals(expected, numberProcessor.getResult());
  }

  @Test
  public void testGetResult_shouldThrowDivisionByZeroException() {
    // given
    int numberOne = 5;
    int numberTwo = 0;
    NumberProcessor numberProcessor = new Divider(numberOne, numberTwo);

    // when
    // then
    assertThrows(DivisionByZeroException.class, numberProcessor::getResult);
  }
}
