main();

function main() {
    const textureUrl = 'cubetexture.png';
    const canvas = document.querySelector('#glcanvas');
    const gl = isCanvasContext(canvas.getContext('webgl'));
    const shaderProgram = initShaderProgram(gl);
    const programInfo = getProgramInfo(shaderProgram, gl);
    const buffers = initBuffers(gl);
    const texture = loadTexture(gl, textureUrl);

    runAnimation(gl, programInfo, buffers, texture);
}
