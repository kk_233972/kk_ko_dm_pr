package pl.tiuckd.processor;

public interface NumberProcessor {

  double getResult();
}
