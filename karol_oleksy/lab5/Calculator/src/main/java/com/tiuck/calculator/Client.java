package com.tiuck.calculator;

import com.tiuck.calculator.operations.Addition;
import com.tiuck.calculator.operations.Division;
import com.tiuck.calculator.operations.Multiplication;
import com.tiuck.calculator.operations.Subtraction;
import com.tiuck.calculator.strategy.OperationStrategy;

import java.util.Scanner;

public class Client {

    private final Calculator calculator;
    private Scanner input;


    public Client() {
        this.calculator = new Calculator();
        this.input = new Scanner(System.in);
    }

    public void run() {

        int operation;
        double firstVariable;
        double secondVariable;

        printInformationText();

        operation = input.nextInt();
        printInputText();
        firstVariable = input.nextDouble();
        printInputText();
        secondVariable = input.nextDouble();
        calculateResult(operation, firstVariable, secondVariable);}

    public void printInformationText(){

        System.out.println("Hello in my calculator. Below you can see instruction how to use it.");
        System.out.println("Select '1' to a+dd two numbers");
        System.out.println("Select '2' to subtract two numbers");
        System.out.println("Select '3' to divide two numbers");
        System.out.println("Select '4' to multiply two numbers");
    }

    public void printInputText(){
        System.out.print("Enter number value: ");
    }

    public void calculateResult(int operation, double firstVariable, double secondVariable) {
        switch (operation) {
            case 1:
                calculator.set(new Addition());
                calculator.countResult(firstVariable, secondVariable);
                break;
            case 2:
                calculator.set(new Subtraction());
                calculator.countResult(firstVariable, secondVariable);
                break;
            case 3:
                calculator.set(new Division());
                calculator.countResult(firstVariable, secondVariable);
                break;
            case 4:
                calculator.set(new Multiplication());
                calculator.countResult(firstVariable, secondVariable);
                break;
        }
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public OperationStrategy getCalculatorStrategy() {
        return calculator.getStrategy();
    }
}
