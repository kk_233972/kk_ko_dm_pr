package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MainTest {

    @Mock
    private Calculator calculator;

    @Test
    void mainTest() {
        when(calculator.evaluateExpression(Float.parseFloat("2.0"), Float.parseFloat("2.0"))).thenReturn(4.0);
        Assertions.assertDoesNotThrow(() -> Main.performCalculation(Logger.getLogger(Main.class.getName()),
                calculator, "2.0", "+", "2.0"));
    }
}