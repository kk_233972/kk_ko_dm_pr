package clean.code;

public class Multiplication implements Operation {
    @Override
    public double evaluate(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }
}
