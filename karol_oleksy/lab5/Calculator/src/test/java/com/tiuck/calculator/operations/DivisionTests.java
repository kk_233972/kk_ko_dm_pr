package com.tiuck.calculator.operations;


import com.tiuck.calculator.operations.Division;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DivisionTests {

    @Test
    public void testCalculate_shouldReturnDivisionOfGivenNumbersTest() {
        Division division = new Division();

        assertEquals(2, division.calculate(10, 5));
    }

    @Test()
    public void shouldReturnDivideBy0Exception() {
        Division division = new Division();

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> division.calculate(5, 0),
                "Cannot divide by 0"
        );
        assertTrue(thrown.getMessage().contains("Cannot divide by 0"));
    }
}
