package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubstractionTest {

  Substraction substraction = new Substraction();
  @Test
  void evaluate() {
    Assertions.assertEquals(0.1, this.substraction.evaluate(1.5, 1.4), 0.01);
  }
}