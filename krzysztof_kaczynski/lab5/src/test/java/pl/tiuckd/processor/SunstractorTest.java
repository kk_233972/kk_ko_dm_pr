package pl.tiuckd.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SunstractorTest {

  @Test
  public void testGetResult_shouldReturnCorrectValue() {
    // given
    int numberOne = 5;
    int numberTwo = 5;
    int expected = numberOne - numberTwo;
    NumberProcessor numberProcessor = new Subtractor(numberOne, numberTwo);

    // when
    // then
    assertEquals(expected, numberProcessor.getResult());
  }
}
