package pl.tiuckd.factory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import pl.tiuckd.exception.IllegalOperationException;
import pl.tiuckd.processor.Adder;
import pl.tiuckd.processor.Divider;
import pl.tiuckd.processor.Multiplier;
import pl.tiuckd.processor.Subtractor;

public class NumberProccessorFactoryTest {

  @Test
  public void testGetCalculator_shouldReturnNumberProcessor() {
    // given
    NumberProccessorFactory numberProccessorFactory = new NumberProccessorFactory();

    // when
    // then
    assertEquals(Adder.class, numberProccessorFactory.getCalculator('+', 0, 0).getClass());
    assertEquals(Subtractor.class, numberProccessorFactory.getCalculator('-', 0, 0).getClass());
    assertEquals(Multiplier.class, numberProccessorFactory.getCalculator('*', 0, 0).getClass());
    assertEquals(Divider.class, numberProccessorFactory.getCalculator('/', 0, 0).getClass());
  }

  @Test
  public void testGetCalculator_shouldThrowException() {
    // given
    NumberProccessorFactory numberProccessorFactory = new NumberProccessorFactory();

    // when
    // then
    assertThrows(
        IllegalOperationException.class, () -> numberProccessorFactory.getCalculator('a', 0, 0));
  }
}
