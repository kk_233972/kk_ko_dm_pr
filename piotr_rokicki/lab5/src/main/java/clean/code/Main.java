package clean.code;

import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Main class
 * @author Piotr, he can't write javadoc
 * @version 1.0
 */
public class Main {
	// Main isn't well covered, bad switch
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Main.class.getName());
        Calculator calculator = new Calculator();
        Scanner inputScanner = new Scanner(System.in);

        String firstNumberString = inputScanner.nextLine();
        String operationString = inputScanner.nextLine();
        String secondNumberString = inputScanner.nextLine();
        performCalculation(logger, calculator, firstNumberString, operationString, secondNumberString);
    }

    public static void performCalculation(Logger logger, Calculator calculator, String firstNumberString, String operationString, String secondNumberString) {
        double firstNumber = Double.parseDouble(firstNumberString);
        double secondNumber = Double.parseDouble(secondNumberString);
        switch (operationString) {
            case "+": {
                calculator.setCurrentOperation(new Addition());
                break;
            }
            case "-": {
                calculator.setCurrentOperation(new Substraction());
                break;
            }
            case "/": {
                calculator.setCurrentOperation(new Division());
                break;
            }
            case "*": {
                calculator.setCurrentOperation(new Multiplication());
                break;
            }
            default: {
                logger.info("Unexpected operator");
                return;
            }
        }
        try {
            logger.info(String.valueOf(calculator.evaluateExpression(firstNumber, secondNumber)));
        } catch (ArithmeticException ex) {
            ex.printStackTrace();
        }
    }
}
