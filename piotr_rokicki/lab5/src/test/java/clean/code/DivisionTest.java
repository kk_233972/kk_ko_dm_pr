package clean.code;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivisionTest {

  Division division = new Division();
  @Test
  void evaluate() {
    Assertions.assertEquals(2.0, this.division.evaluate(8.0, 4.0), 0.01);
  }
  @Test
  void divisionBy0() {
    Assertions.assertThrows(ArithmeticException.class, () -> this.division.evaluate(8.0, 0.0));
  }
}