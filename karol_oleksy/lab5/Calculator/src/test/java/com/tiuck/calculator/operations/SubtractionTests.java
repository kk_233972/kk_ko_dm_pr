package com.tiuck.calculator.operations;

import com.tiuck.calculator.operations.Subtraction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubtractionTests {

    @Test
    public void testCalculate_shouldReturnSubtractionOfGivenNumbers() {
        Subtraction subtraction = new Subtraction();

        assertEquals(-2, subtraction.calculate(3, 5), 2);
    }
}
