package com.tiuck.calculator;

import com.tiuck.calculator.strategy.OperationStrategy;

import java.io.PrintStream;

public class Calculator {
    private OperationStrategy strategy;
    private double result;

    public void set(OperationStrategy strategy) {
        this.strategy = strategy;
    }

    public void countResult(double a, double b) {
        this.result = strategy.calculate(a, b);
        countResult(result);
    }

    public OperationStrategy getStrategy() {
        return strategy;
    }

    public void countResult(double result) {
        System.out.println("Operation result: " + result);
    }

    public double getResult() {
        return result;
    }
}
