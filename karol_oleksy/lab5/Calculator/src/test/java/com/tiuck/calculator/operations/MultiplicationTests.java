package com.tiuck.calculator.operations;

import com.tiuck.calculator.operations.Multiplication;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplicationTests {

    @Test
    public void testCalculate_shouldReturnMultiplicationOfGivenNumbers() {

        Multiplication multiplication = new Multiplication();

        assertEquals(15, multiplication.calculate(3, 5), 2);
    }
}
