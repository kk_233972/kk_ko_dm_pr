class WebGL {
    constructor(context, vertexShaderSource, fragmentShaderSource, buffersService, textureService) {
        this.webGLContext = context;
        this.cubeRotation = 0.0;
        this.animationProgressTimeVariable = 0.0;
        this.vertexShaderSource = vertexShaderSource;
        this.fragmentShaderSource = fragmentShaderSource;
        this.buffersService = buffersService;
        this.textureService = textureService;
    }

    constructedProperly() {
        if (!this.webGLContext) {
            alert('Unable to initialize WebGL. Your browser or machine may not support it.');
            return false
        }
        else {
            return true;
        }
    }

    init() {
        this.buffers = this.buffersService.initBuffers(this.webGLContext);
        this.texture = this.textureService.loadTexture(this.webGLContext, 'cubetexture.png');
        const shaderProgram = new ShaderProgram(this.webGLContext, this.vertexShaderSource, this.fragmentShaderSource);
        this.programInfo = shaderProgram.programInfo;
    }

    clearScene() {
        this.webGLContext.clearColor(0.0, 0.0, 0.0, 1.0);
        this.webGLContext.clearDepth(1.0);
        this.webGLContext.enable(this.webGLContext.DEPTH_TEST);
        this.webGLContext.depthFunc(this.webGLContext.LEQUAL);
        this.webGLContext.clear(this.webGLContext.COLOR_BUFFER_BIT | this.webGLContext.DEPTH_BUFFER_BIT);
    }

    createPerspectiveMatrix() {
        const fieldOfViewInRadians = 45 * Math.PI / 180;
        const aspect = this.webGLContext.canvas.clientWidth / this.webGLContext.canvas.clientHeight;
        const nearestZCoord = 0.1;
        const farthestZCoord = 100.0;
        const perspectiveMatrix = mat4.create();

        mat4.perspective(
            perspectiveMatrix,
            fieldOfViewInRadians,
            aspect,
            nearestZCoord,
            farthestZCoord
        );
        return perspectiveMatrix;
    }

    createModelViewMatrix() {
        const modelViewMatrix = mat4.create();

        mat4.translate(modelViewMatrix,     // destination matrix
            modelViewMatrix,     // matrix to translate
            [-0.0, 0.0, -6.0]);  // amount to translate
        mat4.rotate(modelViewMatrix,  // destination matrix
            modelViewMatrix,  // matrix to rotate
            this.cubeRotation,     // amount to rotate in radians
            [0, 0, 1]);       // axis to rotate around (Z)
        mat4.rotate(modelViewMatrix,  // destination matrix
            modelViewMatrix,  // matrix to rotate
            this.cubeRotation * .7,// amount to rotate in radians
            [0, 1, 0]);       // axis to rotate around (X)
        return modelViewMatrix;
    }

    createNormalMatrix(modelViewMatrix) {
        const normalMatrix = mat4.create();
        mat4.invert(normalMatrix, modelViewMatrix);
        mat4.transpose(normalMatrix, normalMatrix);
        return normalMatrix;
    }

    pullDataFromBufferIntoAttribute(buffer, bufferType) {
        {
            const type = this.webGLContext.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            this.webGLContext.bindBuffer(this.webGLContext.ARRAY_BUFFER, buffer);
            switch (bufferType) {
                case 'vertexPosition': {
                    const numComponents = 3;
                    this.webGLContext.vertexAttribPointer(
                        this.programInfo.attribLocations.vertexPosition,
                        numComponents,
                        type,
                        normalize,
                        stride,
                        offset);
                    this.webGLContext.enableVertexAttribArray(
                        this.programInfo.attribLocations.vertexPosition);
                    break;
                }
                case 'vertexNormal': {
                    const numComponents = 3;
                    this.webGLContext.vertexAttribPointer(
                        this.programInfo.attribLocations.vertexNormal,
                        numComponents,
                        type,
                        normalize,
                        stride,
                        offset);
                    this.webGLContext.enableVertexAttribArray(
                        this.programInfo.attribLocations.vertexNormal);
                    break;
                }
                case 'textureCoord': {
                    const numComponents = 2;
                    this.webGLContext.vertexAttribPointer(
                        this.programInfo.attribLocations.textureCoord,
                        numComponents,
                        type,
                        normalize,
                        stride,
                        offset);
                    this.webGLContext.enableVertexAttribArray(
                        this.programInfo.attribLocations.textureCoord);
                    break;
                }
                default: {
                    console.error(bufferType);
                    alert('Wrong Type. Allowed vertexPosition, vertexNormal, textureCoord')
                }
            }
        }
    }

    setShaderUniforms(perspectiveMatrix, modelViewMatrix, normalMatrix) {
        this.webGLContext.uniformMatrix4fv(
            this.programInfo.uniformLocations.projectionMatrix,
            false,
            perspectiveMatrix);
        this.webGLContext.uniformMatrix4fv(
            this.programInfo.uniformLocations.modelViewMatrix,
            false,
            modelViewMatrix);
        this.webGLContext.uniformMatrix4fv(
            this.programInfo.uniformLocations.normalMatrix,
            false,
            normalMatrix);
    }

    bindTexture() {
        this.webGLContext.activeTexture(this.webGLContext.TEXTURE0);
        this.webGLContext.bindTexture(this.webGLContext.TEXTURE_2D, this.texture);
        this.webGLContext.uniform1i(this.programInfo.uniformLocations.uSampler, 0);
    }

    drawElements() {
        const vertexCount = 36;
        const type = this.webGLContext.UNSIGNED_SHORT;
        const offset = 0;
        this.webGLContext.drawElements(this.webGLContext.TRIANGLES, vertexCount, type, offset);
    }

    drawScene(deltaTime) {
        this.clearScene();

        const perspectiveMatrix = this.createPerspectiveMatrix();
        const modelViewMatrix = this.createModelViewMatrix();
        const normalMatrix = this.createNormalMatrix(modelViewMatrix);

        this.pullDataFromBufferIntoAttribute(this.buffers.position, 'vertexPosition');
        this.pullDataFromBufferIntoAttribute(this.buffers.textureCoord, 'textureCoord');
        this.pullDataFromBufferIntoAttribute(this.buffers.normal, 'vertexNormal');
        this.webGLContext.bindBuffer(this.webGLContext.ELEMENT_ARRAY_BUFFER, this.buffers.indices);

        this.webGLContext.useProgram(this.programInfo.program);

        this.setShaderUniforms(perspectiveMatrix, modelViewMatrix, normalMatrix);
        this.bindTexture();
        this.drawElements();

        this.cubeRotation += deltaTime;
    }

    render(now) {
        var nowInSeconds = now * 0.001;
        const deltaTime = nowInSeconds - this.animationProgressTimeVariable;
        this.animationProgressTimeVariable = nowInSeconds;

        this.drawScene(deltaTime);
        requestAnimationFrame( (now) => this.render(now));
    }

    startDrawing() {
        requestAnimationFrame((now) => this.render(now));
    }
}
