function runAnimation(gl, programInfo, buffers, texture) {
    let then = 0;
    let cubeRotation = 0.0;
    function render(now) {
        now *= 0.001;
        const deltaTime = now - then;
        then = now;
        cubeRotation = drawScene(gl, programInfo, buffers, texture, deltaTime, cubeRotation);
        requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}

function getProgramInfo(shaderProgram, gl) {
    return {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
            vertexNormal: gl.getAttribLocation(shaderProgram, 'aVertexNormal'),
            textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
            normalMatrix: gl.getUniformLocation(shaderProgram, 'uNormalMatrix'),
            uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
        },
    };
}

function isCanvasContext(gl) {
    if (!gl) {
        throw new Error('Unable to initialize WebGL. Your browser or machine may not support it.');
    }
    return gl;
}
