package pl.tiuckd.io.util;

import pl.tiuckd.type.ErrorType;
import pl.tiuckd.type.NumberOrder;

public class CalculatorPrinter {

  public void printHelloMessage() {
    System.out.println("Kalkulator TIUCK - Laboratorium 4\nAutor: Dawid Macias");
  }

  public void printOperationCharacter() {
    System.out.print("Podaj znak operacji: ");
  }

  public void printNumber(NumberOrder numberOrder) {
    System.out.print("Podaj " + numberOrder.getInput() + " liczbe: ");
  }

  public void printResultMessage(double result) {
    System.out.println("Wynikiem operacji jest liczba: " + result);
  }

  public void printErrorMessage(ErrorType errorType) {
    String message = "BLAD! ";
    switch (errorType) {
      case DIVISION:
        message = message + "Dzielenie przez zero!";
        break;
      case NUMBER:
        message = message + "Nieprawidlowa liczba!\nSprobuj jeszcze raz";
        break;
      case OPERATION:
        message = message + "Nieprawidlowa operacja!";
        break;
    }
    System.out.println(message);
  }
}
