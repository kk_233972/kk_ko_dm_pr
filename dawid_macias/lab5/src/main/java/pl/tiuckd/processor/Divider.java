package pl.tiuckd.processor;

import pl.tiuckd.exception.DivisionByZeroException;

public class Divider implements NumberProcessor {

  private final double numberOne;
  private final double numberTwo;

  public Divider(double numberOne, double numberTwo) {
    this.numberOne = numberOne;
    this.numberTwo = numberTwo;
  }

  @Override
  public double getResult() {
    if (numberTwo == 0) {
      throw new DivisionByZeroException();
    }
    return numberOne / numberTwo;
  }
}
