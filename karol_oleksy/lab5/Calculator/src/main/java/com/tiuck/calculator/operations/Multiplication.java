package com.tiuck.calculator.operations;

import com.tiuck.calculator.strategy.OperationStrategy;

public class Multiplication implements OperationStrategy {

    @Override
    public double calculate(double a, double b) {
        return a * b;
    }
}
