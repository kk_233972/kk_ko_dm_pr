package com.tiuck.calculator;

import com.tiuck.calculator.operations.Addition;
import com.tiuck.calculator.operations.Division;
import com.tiuck.calculator.operations.Multiplication;
import com.tiuck.calculator.operations.Subtraction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClientTests {

    @Test
    public void testConstructor_shouldReturnThatObjectIsInstanceOfClient() {
        Client client = new Client();
        assertTrue(client instanceof Client);
    }

    @Test
    public void testConstructor_shouldReturnThatObjectIsInstanceOfCalculator() {
        Client client = new Client();
        assertTrue(client.getCalculator() instanceof Calculator);
    }

    @Test
    public void testCalculate_shouldReturnThatObjectIsInstanceAddition() {
        Client client = new Client();
        client.calculateResult(1, 3, 5);
        assertTrue(client.getCalculatorStrategy() instanceof Addition);
    }

    @Test
    public void testCalculate_shouldReturnThatObjectIsInstanceSubtraction() {
        Client client = new Client();
        client.calculateResult(2, 3, 5);
        assertTrue(client.getCalculatorStrategy() instanceof Subtraction);
    }

    @Test
    public void testCalculate_shouldReturnThatObjectIsInstanceDivision() {
        Client client = new Client();
        client.calculateResult(3, 3, 5);
        assertTrue(client.getCalculatorStrategy() instanceof Division);
    }

    @Test
    public void testCalculate_shouldReturnThatObjectIsInstanceMultiplication() {
        Client client = new Client();
        client.calculateResult(4, 3, 5);
        assertTrue(client.getCalculatorStrategy() instanceof Multiplication);
    }

    @Test
    public void testPrintInformation_shouldReturnInformationText() {
        Client client = new Client();
        client.printInformationText();
    }

    @Test
    public void testPrintInputText_shouldReturnInputText() {
        Client client = new Client();
        client.printInputText();
    }
}
