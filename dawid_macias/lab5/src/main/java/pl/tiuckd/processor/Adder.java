package pl.tiuckd.processor;

public class Adder implements NumberProcessor {

  private final double numberOne;
  private final double numberTwo;

  public Adder(double numberOne, double numberTwo) {
    this.numberOne = numberOne;
    this.numberTwo = numberTwo;
  }

  @Override
  public double getResult() {
    return numberOne + numberTwo;
  }
}
