class BuffersService {
    constructor(shape) {
        this.vertexPositions = shape.vertexPositions;
        this.vertexNormals = shape.vertexNormals;
        this.textureCoordinates = shape.textureCoordinates;
        this.indices = shape.indices;
    }

    initBuffer(gl, dataArray, isFloat32) {

        const buffer = gl.createBuffer();
        if (isFloat32) {
            gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        } else {
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
        }

        if (isFloat32) {
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(dataArray), gl.STATIC_DRAW);
        } else {
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
                new Uint16Array(dataArray), gl.STATIC_DRAW);
        }

        return buffer;
    }

    initBuffers(gl) {

        const verticesPositionBuffer = this.initBuffer(gl, this.vertexPositions, true);
        const verticesNormalBuffer = this.initBuffer(gl, this.vertexNormals, true);
        const textureCoordBuffer = this.initBuffer(gl, this.textureCoordinates, true);
        const indexBuffer = this.initBuffer(gl, this.indices, false);

        return {
            position: verticesPositionBuffer,
            normal: verticesNormalBuffer,
            textureCoord: textureCoordBuffer,
            indices: indexBuffer,
        };
    }
}


