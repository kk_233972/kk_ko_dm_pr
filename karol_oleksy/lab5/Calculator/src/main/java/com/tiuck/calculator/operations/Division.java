package com.tiuck.calculator.operations;

import com.tiuck.calculator.strategy.OperationStrategy;

public class Division implements OperationStrategy {

    @Override
    public double calculate(double a, double b) {
        if(b == 0)
            throw new IllegalArgumentException("Cannot divide by 0");
        return a / b;
    }
}
